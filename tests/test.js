var assert = require('assert');
var fs = require('fs')
var copy = require('./../src/copy.js')

describe('andre_copy', function() {
  describe('#copy()', function() {
    it('should write file defined in data file', function(done) {
      copy('tests/data.json', function (err) {
        if (err) throw err

        else assert.ok(fs.existsSync('tests/target/file'))
        done()
      })
    });
  });
});
