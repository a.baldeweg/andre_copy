# Changelog

# v2.0.0

- cleaned up .editorconfig
- updated dependencies
- improvements

# v1.0.2

- moved to gitlab

# v1.0.1

- replaced makfile by shell scripts
- improvements
- switched to yarn

# v1.0.0

- options work now like a router
- [BC] calling `andre_copy --data data.json` instead of `andre_copy data.json`
- it's now possible to generate the config file with `andre_copy --generate` automatically
- some improvements

# v0.1.1

- added mocha for tests

# v0.1.0

- First release
