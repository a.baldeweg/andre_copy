# andre_copy

Script to read a list of files and copy them using glob from one dir to another.

## Installation

Install the package with npm by running `npm install andre_copy` or yarn `yarn add andre_copy`.

## Usage

If installed globally just omit the `node_modules/.bin/` in the following examples.

Run `node_modules/.bin/andre_copy --data data.json`

where `data.json` could look similar to this example:

```json
{
  "files": [
    [
      "dir/*",
      "target"
    ],
    [
      "file",
      "target"
    ]
  ]
}
```

It's possible to generate a template of the config file with `andre_copy --generate` automatically.

No matter where to store the file or which name it has, just move or rename it and edit the command according your needs.

You can use files and globs, so copy a particular file or e.g. all JavaScript files from one dir to another.

## Dev

- Clone the git repo with `git clone https://gitlab.com/a.baldeweg/andre_copy.git`
- run `npm install` or `yarn`

## Code Standard Fixer

- Run `bin/csfixer`

## Testing

- Run `bin/test`
