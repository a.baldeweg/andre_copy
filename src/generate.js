#!/usr/bin/env node

'use strict'

var fs = require('fs')

function FileExistsException () {
  this.name = 'error'
  this.message = 'File already exists!'
}

function generate () {
  if (fs.existsSync('data.json')) throw new FileExistsException()

  fs.writeFileSync(
    'data.json',
    '{\r\n  "files": [\r\n    [\r\n      "source_files",\r\n      "target_dir"\r\n    ]\r\n  ]\r\n}\r\n'
  )
}

module.exports = generate
