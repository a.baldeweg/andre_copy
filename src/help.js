#!/usr/bin/env node

'use strict'

function showHelp () {
  console.log('andre_copy')
  console.log('Run `andre_copy --data data.json` or if installed locally `node_modules/.bin/andre_copy --data data.json`.')
}

module.exports = showHelp
