#!/usr/bin/env node

'use strict'

var help = require('./help.js')
var copy = require('./copy.js')
var generate = require('./generate.js')

var opt = process.argv[2]
var value = process.argv[3]

switch (opt) {
  case '--data':
    copy(value)
    break
  case '--generate':
    generate()
    break
  default:
    help()
}
