#!/usr/bin/env node

'use strict'

var path = require('path')
var fs = require('fs-extra')
var glob = require('glob')

function copy (arg, callback) {
  fs.readFile(arg, (err, data) => {
    if (err) throw err

    doCopy(JSON.parse(data), path.dirname(arg), function (err) {
      if (!err && callback) callback()
    })
  })
}

function doCopy (data, basedir, callback) {
  for (var item in data.files) {
    var matches = glob.sync(path.normalize(basedir) + '/' + path.normalize(data.files[item][0]))
    var target = path.normalize(basedir) + '/' + path.normalize(data.files[item][1])
    for (var file in matches) {
      fs.copy(matches[file], target + '/' + path.basename(matches[file]), function (err) {
        if (err) throw err
        else callback(err)
      })
    }
  }
}

module.exports = copy
